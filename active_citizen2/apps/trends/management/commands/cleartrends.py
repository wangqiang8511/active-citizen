from django.core.management.base import BaseCommand
from trends.models import Keyword

class Command(BaseCommand):
    help = 'Clear all trends keyword'

    def handle(self, *args, **options):
        Keyword.objects.all().delete()

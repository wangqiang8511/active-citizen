from dajaxice.decorators import dajaxice_register
from trends.models import UserKeyword

@dajaxice_register
def add_user_keyword(request, text):
        if unicode(text) not in (kw.value for kw in UserKeyword.objects.all()):
            keyword = UserKeyword(count=1, value=unicode(text))
            keyword.save()
        else:
            keyword = UserKeyword.objects.get(value=unicode(text))
            keyword.count += 1
            keyword.save()


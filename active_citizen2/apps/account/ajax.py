from dajaxice.decorators import dajaxice_register
from django.utils import simplejson
from account.models import SearchString

@dajaxice_register(method='GET')
def get_search_strings(request):
    try:
        return simplejson.dumps({'message': 'Hello, ' + request.user.username,
            'strings': simplejson.dumps([str.search_string for str in request.user.searchstring_set.all()])})
    except SearchString.DoesNotExist:
        return simplejson.dumps({'message': 'Error: Search strings do not exist in database',
            'strings': simplejson.dumps([])})

@dajaxice_register
def set_search_strings(request, options):
    SearchString.objects.filter(user_id=request.user.id).delete()
    for option in options:
        str = SearchString(user_id=request.user.id, search_string=option)
        str.save()


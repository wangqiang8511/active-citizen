--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: geo_tweets; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE geo_tweets (
    favorited boolean,
    truncated boolean,
    id text NOT NULL,
    text text,
    screenname text,
    created timestamp without time zone
);


ALTER TABLE public.geo_tweets OWNER TO dev;

--
-- Name: geo_tweets_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY geo_tweets
    ADD CONSTRAINT geo_tweets_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--


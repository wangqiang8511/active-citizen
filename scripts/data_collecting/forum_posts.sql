SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE TABLE forum_posts (
    source text,
    id text NOT NULL,
	author text NOT NULL,
    published timestamp without time zone,
    text text
);


ALTER TABLE public.forum_posts OWNER TO dev;


ALTER TABLE ONLY forum_posts
    ADD CONSTRAINT forum_posts_pkey PRIMARY KEY (id);

#!/usr/bin/env python
import os, sys

def setup_environ(dunder_file=None, project_path=None, relative_project_path=None, settings_path=None):
    assert not (dunder_file and project_path), ("You must not specify both "
        "__file__ and project_path")
    
    if dunder_file is not None:
        file_path = os.path.abspath(os.path.dirname(dunder_file))
        if relative_project_path is not None:
            project_path = os.path.abspath(os.path.join(file_path, *relative_project_path))
        else:
            project_path = file_path
    
    # Pinax adds an app directory for users as a reliable location for
    # Django apps
    sys.path.insert(0, os.path.join(project_path, "active_citizen2/apps"))

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "active_citizen2.settings")
    
    from django.core.management import execute_from_command_line
    
    setup_environ(__file__)
    #print >> sys.stderr, sys.path

    execute_from_command_line(sys.argv)
